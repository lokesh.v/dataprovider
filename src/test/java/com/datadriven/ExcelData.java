package com.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ExcelData {
  @DataProvider
  public String [][] getdata() throws IOException {
	  File f=new File("/home/lokesh/Documents/TestData/TestData3.xlsx");
		
		FileInputStream fis=new FileInputStream(f);
		
		XSSFWorkbook book=new XSSFWorkbook(fis);
		
		XSSFSheet sh=book.getSheetAt(0);
		
		int rows=sh.getPhysicalNumberOfRows();
		int columns=sh.getRow(0).getLastCellNum();
		
		String[][] data= new String[rows-1][columns];
		for (int i = 0; i < rows-1; i++) {
			for (int j = 0; j < columns; j++) {
				DataFormatter df=new DataFormatter();
				data[i][j]=df.formatCellValue(sh.getRow(i+1).getCell(j));
			}
			
		}
	return data;
	  
  }
}
