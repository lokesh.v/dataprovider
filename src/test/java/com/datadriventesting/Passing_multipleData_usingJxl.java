package com.datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Passing_multipleData_usingJxl {

	public static void main(String[] args) throws BiffException, IOException {
		// TODO Auto-generated method stub
      
		WebDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/");
		File f= new File("/home/lokesh/Documents/TestData/TestData1.xls");
		FileInputStream fis=new FileInputStream(f);
		Workbook book=Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("Sheet1");
//		String celldata=sh.getCell(1,2).getContents();
//		System.out.println("VAlue Present Is :"+celldata);
			
			int rows =sh.getRows();
			int columns=sh.getColumns();
			
			for(int i=1;i<rows;i++) {
				String Username=sh.getCell(0, i).getContents();
				String Password=sh.getCell(1, i).getContents();
				
			
		driver.manage().window().maximize();
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys(Username);
		driver.findElement(By.id("Password")).sendKeys(Password);
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		driver.findElement(By.linkText("Log out")).click();
			}
	  
	
	}

}
