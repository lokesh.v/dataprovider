package com.datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class MultipleDataAssignmentUsing_Jxl {

	public static void main(String[] args) throws BiffException, IOException {
		// TODO Auto-generated method stub

		WebDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/");
		
		File f=new File("/home/lokesh/Documents/TestData/Register.xls");
		
		FileInputStream fis=new FileInputStream(f);
        Workbook book=Workbook.getWorkbook(fis);
        Sheet sh=book.getSheet("Sheet1");
        
        int rows=sh.getRows();
        int columns=sh.getColumns();
        
        for(int i=1;i<rows;i++) {
        	String Firstname=sh.getCell(0, i).getContents();
        	String Lastname=sh.getCell(1, i).getContents();
        	String Email=sh.getCell(2, i).getContents();
        	String Password=sh.getCell(3,i).getContents();
        	
        	
        	driver.findElement(By.linkText("Register")).click();
        	driver.findElement(By.id("gender-male")).click();
        	driver.findElement(By.id("FirstName")).sendKeys(Firstname);
        	driver.findElement(By.id("LastName")).sendKeys(Lastname);
        	driver.findElement(By.id("Email")).sendKeys(Email);
        	driver.findElement(By.id("Password")).sendKeys(Password);
        	driver.findElement(By.id("ConfirmPassword")).sendKeys(Password);
        	driver.findElement(By.id("register-button")).click();
        	driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
        	driver.findElement(By.linkText("Log out")).click();
        	
        	
        	
        }
        driver.close();
        
	}

}
