package com.datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class MultipleDataAssignmentUsing_XlSx {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		WebDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/");
		
		
	File f=new File("/home/lokesh/Documents/TestData/REgister.xlsx");
		
		FileInputStream fis=new FileInputStream(f);
		
		XSSFWorkbook book=new XSSFWorkbook(fis);
		
		XSSFSheet sh=book.getSheetAt(0);
		
		int rows=sh.getPhysicalNumberOfRows();
		
		for (int i = 1; i < rows; i++) {
			
				String Firstname=sh.getRow(i).getCell(0).getStringCellValue();
	        	String Lastname=sh.getRow(i).getCell(1).getStringCellValue();
	        	String Email=sh.getRow(i).getCell(2).getStringCellValue();
	        	String Password=sh.getRow(i).getCell(3).getStringCellValue();
	        	driver.findElement(By.linkText("Register")).click();
	        	driver.findElement(By.id("gender-male")).click();
	        	driver.findElement(By.id("FirstName")).sendKeys(Firstname);
	        	driver.findElement(By.id("LastName")).sendKeys(Lastname);
	        	driver.findElement(By.id("Email")).sendKeys(Email);
	        	driver.findElement(By.id("Password")).sendKeys(Password);
	        	driver.findElement(By.id("ConfirmPassword")).sendKeys(Password);
	        	driver.findElement(By.id("register-button")).click();
	        	driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
	        	driver.findElement(By.linkText("Log out")).click();
	        	
	        	
			}
			
		}
		
		
	}


