package com.datadriventesting;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Passing_multipleDataToLoginUsingApachePoi {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
    
		
		WebDriver driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.get("https://demowebshop.tricentis.com/");
		
		File f=new File("/home/lokesh/Documents/TestData/TestData2.xlsx");
		
		FileInputStream fis=new FileInputStream(f);
		
		XSSFWorkbook book=new XSSFWorkbook(fis);
		
		XSSFSheet sh=book.getSheetAt(0);
		
		int rows=sh.getPhysicalNumberOfRows();
		
		for (int i = 1; i < rows; i++) {
			
			String Username=sh.getRow(i).getCell(0).getStringCellValue();
			String Password=sh.getRow(i).getCell(1).getStringCellValue();
			
			driver.findElement(By.linkText("Log in")).click();
			driver.findElement(By.id("Email")).sendKeys("lokeshbabu@gmail.com");
			driver.findElement(By.id("Password")).sendKeys("Lokessh");
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			driver.findElement(By.linkText("Log out")).click();
			}
			
		}
		
	}


