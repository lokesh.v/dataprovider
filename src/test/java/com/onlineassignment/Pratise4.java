package com.onlineassignment;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Pratise4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  WebDriver driver=new ChromeDriver();
	       driver.manage().window().maximize();
	       driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	       driver.get("https://testsheepnz.github.io/BasicCalculator.html");
	       
	       JavascriptExecutor js= (JavascriptExecutor)driver;
	       js.executeScript("document.getElementById('number1Field').scrollIntoView()");
	       driver.close();
	}

}
