package com.onlineassignment;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Pratice2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
       WebDriver driver=new ChromeDriver();
       driver.manage().window().maximize();
       driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
       driver.get("https://demo.automationtesting.in/Alerts.html");
       driver.findElement(By.xpath("//a[text()='Alert with OK & Cancel ']")).click();
	   driver.findElement(By.xpath("//button[@onclick='confirmbox()']")).click();
	   driver.switchTo().alert().accept();
	   WebElement a=driver.findElement(By.id("demo"));
	   System.out.println(a.getText());
	}

}
